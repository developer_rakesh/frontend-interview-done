export interface SnackBarProps {
  isShowPopup: boolean;
  message: string;
  onClose: () => void;
}