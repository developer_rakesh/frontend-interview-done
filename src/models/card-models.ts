﻿export enum Priority {
  Error,
  Warn,
  Info,
}

export interface Message {
  message: string;
  priority: Priority;
}

export interface CardProps {
  messages: any;
  clearMeHandler: (index: number) => void;
}

export interface PriorityCardListProps {
  cardsListBody: any;
  type: Priority;
  clearMeHandler: (index: number) => void;
}

export interface CardHeaderProps {
  cardsListBody: any;
  type: Priority;
  header: string;
}

export interface Props {
  msg: string;
  priority: Priority;
  index: number;
  clearMeHandler: (index: number) => void;
}

