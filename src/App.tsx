import React, { useState } from "react";
import { useEffect } from "react";
import generateMessage from "./Api";
import CardList from "./components/_card/CardList";
import { useStylesApp } from "./AppStyle";
import { SnackbarModal } from "./components/_modals/SnackbarModal/SnackBarModal";
import { Message, Priority } from "./models/card-models";
import {
  Button,
  Grid,
  Container,
  AppBar,
  Toolbar,
  Typography,
} from "@material-ui/core";

const App: React.FC<{}> = () => {
  const classes = useStylesApp();
  const [messages, setMessages] = useState<Message[]>([]);
  const [isStop, setStop] = useState(false);
  const [snackbarMsg, setSnackbarMsg] = useState("");
  const [isShowPopup, setIsShowPopup] = useState(false);

  useEffect(() => {
    if (isStop === false) {
      const cleanUp = generateMessage((message: Message) => {
        if (message.priority === Priority.Error) {
          setIsShowPopup(true);
          setSnackbarMsg(message.message);
        }
        setMessages((oldMessages) => [message, ...oldMessages]);
      });
      return cleanUp;
    }
  }, [isStop, setMessages]);

  // stop/start getting messages from API
  const stopHandler = () => {
    setStop(!isStop); // toggle
  };

  // clear all messages
  const clearHandler = () => {
    setMessages([]);
  };

  // show and hide snackbar
  useEffect(() => {
    if (isShowPopup) {
      setIsShowPopup(true);
      setTimeout(() => {
        setIsShowPopup(false);
      }, 2000);
    }
  }, [isShowPopup]);

  const handleClose = () => {
    setIsShowPopup(false);
  };

  // Clear perticular message
  const clearMeHandler = (index: number) => {
    setMessages(messages.filter((x: any, i) => i !== index));
  };

  return (
    <>
      <div>
        <SnackbarModal
          isShowPopup={isShowPopup}
          onClose={handleClose}
          message={snackbarMsg}
        />
      </div>

      <AppBar className={classes.nav}>
        <Toolbar>
          <Container>
            <Typography variant="h5">Coding Challenge</Typography>
          </Container>
        </Toolbar>
      </AppBar>
      <Container>
        <div className={classes.root}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Button />
            <Button
              variant="contained"
              className={classes.btn}
              onClick={() => stopHandler()}
            >
              {isStop ? "Start" : "Stop"}
            </Button>
            <Button
              variant="contained"
              onClick={() => clearHandler()}
              className={classes.btn}
              type="button"
            >
              Clear
            </Button>
          </Grid>

          <CardList messages={messages} clearMeHandler={clearMeHandler} />
        </div>
      </Container>
    </>
  );
};

export default App;
