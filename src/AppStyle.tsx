import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

export const useStylesApp = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    nav: {
      background: "#f3f3f3",
      color: "#000000",
    },
    btn: {
      textAlign: "center",
      color: theme.palette.text.secondary,
      backgroundColor: "#88FCA3",
      fontWeight: "bold",
      marginRight: 8,
      marginTop: 70,
    },
  })
);
