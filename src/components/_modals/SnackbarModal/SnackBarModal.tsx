import { Button, Snackbar } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import { SnackBarProps } from "../../../models/snackbar-models";

const SnackbarModal = ({ isShowPopup, message, onClose }: SnackBarProps) => {
  const action = (
    <>
      <Button color="secondary" size="small" onClick={onClose}>
        <CloseIcon />
      </Button>
    </>
  );

  return (
    <>
      <Snackbar
        style={{ marginTop: "70px" }}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        open={isShowPopup}
        onClose={onClose}
        message={message}
        action={action}
      />
    </>
  );
};

export { SnackbarModal };
