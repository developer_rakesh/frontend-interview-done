import React from "react";
import { Typography, ListItem, List } from "@material-ui/core";
import { CardHeaderProps } from "../../models/card-models";
const CardHeader = ({
  cardsListBody,
  type,
  header,
}: CardHeaderProps) => {
  return (
    <div>
      <List>
        <ListItem>
          <Typography variant="h4">
            {header}
            <Typography>
              <span style={{ fontWeight: "bold" }}> count </span> &nbsp;
              {cardsListBody.filter((x: any) => x.priority === type).length}
            </Typography>
          </Typography>
        </ListItem>
      </List>
    </div>
  );
};

export { CardHeader };
