import { makeStyles, createStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(() =>
  createStyles({
    btn: {
      float: "right",
    },
    error: {
      backgroundColor: "#F56236",
    },

    warn: {
      backgroundColor: "#FCE788",
    },
    info: {
      backgroundColor: "#88FCA3",
    },
    typography: {
      maxHeight: "23px",
    },
    cardWIdth: {
      width: "100%",
    },
  })
);

export const useStylesCardList = makeStyles(() =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    contain: {
      marginTop: "50px",
    },
    btn: {
      float: "right",
    },
  })
);
