import React from "react";
import { Grid, ListItem, List } from "@material-ui/core";
import { useStyles } from "./CardStyle";
import CardMessage from "./CardMessage";
import {
  Message,
  PriorityCardListProps,
} from "../../models/card-models";

const PriorityCardList = ({
  cardsListBody,
  type,
  clearMeHandler,
}: PriorityCardListProps) => {
  const classes = useStyles();
  return (
    <div className={classes.cardWIdth}>
      <List>
        {cardsListBody?.map(
          (card: Message, i: number) =>
            card?.priority === type && (
              <ListItem key={i}>
                <Grid item xs={12}>
                  <CardMessage
                    msg={card?.message}
                    priority={card?.priority}
                    index={i}
                    clearMeHandler={clearMeHandler}
                  />
                </Grid>
              </ListItem>
            )
        )}
      </List>
    </div>
  );
};

export { PriorityCardList };
