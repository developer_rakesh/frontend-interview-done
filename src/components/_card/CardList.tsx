import { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import { Priority, CardProps } from "../../models/card-models";
import { PriorityCardList } from "./PriorityCardList";
import { CardHeader } from "./CardHeader";
import { useStylesCardList } from "./CardStyle";

const CardList = ({ messages, clearMeHandler }: CardProps) => {
  const classes = useStylesCardList();
  const [cards, setCards] = useState([]);

  useEffect(() => {
    const allCards = messages;
    setCards(allCards);
  }, [messages]);

  return (
    <div className={classes.root}>
      <Grid container spacing={1} className={classes.contain}>
        <Grid container item md={4}>
          <CardHeader
            cardsListBody={cards}
            type={Priority.Error}
            header="Error Type 1"
          />
        </Grid>
        <Grid container item md={4}>
          <CardHeader
            cardsListBody={cards}
            type={Priority.Warn}
            header="Warning Type 2"
          />
        </Grid>
        <Grid container item md={4}>
          <CardHeader
            cardsListBody={cards}
            type={Priority.Info}
            header="Info Type 3"
          />
        </Grid>
        <Grid container item md={4}>
          <PriorityCardList
            cardsListBody={cards}
            type={Priority.Error}
            clearMeHandler={clearMeHandler}
          />
        </Grid>
        <Grid container item xs={4}>
          <PriorityCardList
            cardsListBody={cards}
            type={Priority.Warn}
            clearMeHandler={clearMeHandler}
          />
        </Grid>
        <Grid container item xs={4}>
          <PriorityCardList
            cardsListBody={cards}
            type={Priority.Info}
            clearMeHandler={clearMeHandler}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default CardList;
