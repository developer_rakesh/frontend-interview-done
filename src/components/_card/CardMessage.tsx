import { Props, Priority } from "../../models/card-models";
import { useStyles } from "./CardStyle";
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
} from "@material-ui/core";

const CardMessage = ({ clearMeHandler, msg, index, priority }: Props) => {
  const classes = useStyles();

  const handleUserClick = (index: number) => {
    clearMeHandler(index);
  };

  return (
    <>
      {
        <Card
          className={
            priority === Priority.Error
              ? classes.error
              : priority === Priority.Warn
                ? classes.warn
                : priority === Priority.Info
                  ? classes.info
                  : ""
          }
          variant="outlined"
        >
          <CardContent>
            <Typography className={classes.typography}>{msg}</Typography>
          </CardContent>
          <CardActions className={classes.btn}>
            <Button size="small" onClick={() => handleUserClick(index)}>
              Clear
            </Button>
          </CardActions>
        </Card>
      }
    </>
  );
};

export default CardMessage;
